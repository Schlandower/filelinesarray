var fs = require('fs');
var oseol = require('os').EOL;

module.exports = function (filename) {
	var buffer = fs.readFileSync(filename);
	var f = buffer.toString();
	return f.split(oseol);
};
